const WebSocket = require('ws');

const wss = new WebSocket.Server({port: 3030});

wss.on('connection', (ws: any) => {
  ws.on('message', (data: any) => {
    wss.clients.forEach((client: any) => {
      if (client !== ws && client.readyState === WebSocket.OPEN) {
        client.send(data);
      }
    });
  });
});

export {};
