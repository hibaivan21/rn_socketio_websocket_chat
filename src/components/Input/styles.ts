import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  inputContainer: {
    height: 50,
    borderWidth: 2,
    marginHorizontal: 25,
    borderRadius: 10,
    width: '90%',
  },
  label: {
    fontSize: 19,
    marginBottom: 20,
  },
});
