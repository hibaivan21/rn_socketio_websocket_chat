import React from 'react';
import {Text, TextInput, View} from 'react-native';
import styles from './styles';

interface InputProps {
  value: string;
  onChangeText: (event: any) => void;
}

const Input = (props: InputProps) => (
  <View style={styles.container}>
    <Text style={styles.label}>user name: {props.value}</Text>
    <TextInput
      testID="input-test"
      style={styles.inputContainer}
      autoCorrect={false}
      value={props.value}
      onChangeText={props.onChangeText}
      textAlign="center"
    />
  </View>
);

export default Input;
