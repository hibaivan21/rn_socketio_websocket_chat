import React from 'react';
import renderer from 'react-test-renderer';
import Input from './index';
import {render, fireEvent} from '@testing-library/react-native';

it('render without crashes', () => {
  const rendered = renderer.create(<Input />).toJSON();
  expect(rendered).toBeTruthy();
});

test('it changes the value in <TextInput />', () => {
  const onChangeTextMock = jest.fn();
  const {getByTestId} = render(
    <Input value="" onChangeText={onChangeTextMock} />,
  );
  const testValue = 'test value';
  const input = getByTestId('input-test');
  fireEvent.changeText(input, testValue);
  expect(input).toBeTruthy();
});
