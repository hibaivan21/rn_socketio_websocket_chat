import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: '#000',
    height: 45,
    flex: 1 / 2,
    marginHorizontal: 20,
    marginTop: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  title: {
    color: '#fff',
    fontSize: 19,
  },
});
