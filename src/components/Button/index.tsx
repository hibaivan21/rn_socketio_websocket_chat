import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import styles from './styles';

interface ButtonProps {
  title: string;
  onPress: (event: any) => void;
  disabled?: boolean;
}

const Button = (props: ButtonProps) => (
  <TouchableOpacity
    disabled={props.disabled}
    onPress={props.onPress}
    style={styles.container}>
    <Text style={styles.title}>{props.title}</Text>
  </TouchableOpacity>
);

export default Button;
