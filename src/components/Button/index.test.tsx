import 'react-native';
import {configure, shallow} from 'enzyme';
import React from 'react';
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import Button from './index';

configure({adapter: new Adapter()});

it('render without crashes', () => {
  const rendered = renderer
    .create(<Button title="test" onPress={() => console.log('test')} />)
    .toJSON();
  expect(rendered).toBeTruthy();
});

test('test button onPress', () => {
  const onPressEvent = jest.fn();
  onPressEvent.mockReturnValue('Link on press invoked');
  const wrapper = shallow(<Button onPress={onPressEvent} title="test" />);
  console.log(wrapper);
});
