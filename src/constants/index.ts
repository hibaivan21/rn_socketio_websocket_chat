export const connectionTypes = {io: 'io', ws: 'ws'};
export const socketIoUrl = 'http://localhost:8000';
export const webSocketUrl = 'ws://localhost:3030';
