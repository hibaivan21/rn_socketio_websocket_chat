import React, {useState} from 'react';
import {Alert, Text, View} from 'react-native';
import 'react-native-get-random-values';
import {v4 as uuid} from 'uuid';
import Button from '../../components/Button';
import Input from '../../components/Input';
import styles from './styles';
import {connectionTypes} from '../../constants';

interface User {
  _id: string;
  name: string;
}

const emptyUser = {
  _id: uuid(),
  name: '',
};

const Home = (props: any) => {
  const [user, setUser] = useState<User>(emptyUser);

  const onPressConnect = (connectionType: string) => {
    if (user.name) {
      props.navigation.navigate('Chat', {
        connectionType,
        user,
      });
    } else {
      Alert.alert('Enter the name first');
    }
  };

  return (
    <View style={styles.container}>
      <Input
        value={user.name}
        onChangeText={(name) => setUser({...user, name})}
      />
      <Text style={styles.buttonLabel}>Connect through:</Text>
      <View style={styles.buttonContainer}>
        <Button
          title={'socket-io'}
          onPress={() => onPressConnect(connectionTypes.io)}
        />
        <Button
          title={'websockets'}
          onPress={() => onPressConnect(connectionTypes.ws)}
        />
      </View>
    </View>
  );
};

export default Home;
