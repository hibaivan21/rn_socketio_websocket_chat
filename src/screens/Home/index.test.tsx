import React from 'react';
import renderer from 'react-test-renderer';
import Home from './index';

it('render without crashes', () => {
  const rendered = renderer.create(<Home />).toJSON();
  expect(rendered).toBeTruthy();
});
