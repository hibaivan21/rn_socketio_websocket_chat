import React, {useEffect, useState} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import io from 'socket.io-client';
import {useRoute} from '@react-navigation/native';
import 'react-native-get-random-values';
import {v4 as uuid} from 'uuid';
import {connect} from 'react-redux';
import {ChatState} from '../../redux/reducer';
import {connectionTypes, socketIoUrl, webSocketUrl} from '../../constants';
import {newMessage} from '../../redux/actions';

export interface Message {
  _id: string;
  user: object;
  createdAt: Date;
  text: string;
}

const emptyMessage: Message = {
  _id: uuid(),
  text: '',
  createdAt: new Date(),
  user: {},
};

const ws = new WebSocket(webSocketUrl);
const socket = io(socketIoUrl);

const Chatroom = (props: any) => {
  const route = useRoute();
  const {user, connectionType}: any = route.params;
  const [message, setMessage] = useState<Message>({...emptyMessage});

  useEffect(() => {
    if (connectionType === connectionTypes.io) {
      startSocketIO();
    } else {
      startWebSocket();
    }
  }, []);

  const startWebSocket = () => {
    ws.onmessage = (e) => {
      const msg = JSON.parse(e.data);
      handleReceive(msg);
    };

    ws.onclose = (e) => {
      console.log('Reconnecting: ', e.message);
      setTimeout(startWebSocket, 5000);
    };
  };

  const startSocketIO = () => {
    socket.on('chat message', (receivedMsg: Message) => {
      handleReceive(receivedMsg);
    });
  };

  const handleReceive = (receivedMsg: Message) => {
    props.newMessage(receivedMsg);
  };

  const handleSend = () => {
    const msg = {...message, _id: uuid()};
    if (connectionType === connectionTypes.io) {
      socket.emit('chat message', msg);
    } else {
      ws.send(JSON.stringify(msg));
      props.newMessage(msg);
    }
    setMessage(emptyMessage);
  };

  const handleChangeText = (text: string) => {
    setMessage({
      ...message,
      _id: uuid(),
      text,
      createdAt: new Date(),
      user,
    });
  };

  return (
    <GiftedChat
      messages={props.messages}
      onSend={handleSend}
      onInputTextChanged={handleChangeText}
      user={{
        _id: user._id,
      }}
    />
  );
};

const mapStateToProps = (state: ChatState) => {
  return {
    messages: state.messages,
  };
};

export default connect(mapStateToProps, {newMessage})(Chatroom);
