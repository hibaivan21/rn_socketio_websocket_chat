import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Home from './screens/Home';
import Chatroom from './screens/Chatroom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducer from './redux/reducer';
import thunk from 'redux-thunk';

export const store = createStore(reducer, applyMiddleware(thunk));

const Stack = createStackNavigator();

const Router = () => (
  <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name={'Home'} component={Home} />
        <Stack.Screen name={'Chat'} component={Chatroom} />
      </Stack.Navigator>
    </NavigationContainer>
  </Provider>
);

export default Router;
