import {Message} from '../screens/Chatroom';
import types from './types';

interface SendMessageAction {
  type: typeof types.SET_NEW_MESSAGE;
  payload: Message;
}

export function newMessage(message: Message): SendMessageAction {
  return {
    type: types.SET_NEW_MESSAGE,
    payload: message,
  };
}
