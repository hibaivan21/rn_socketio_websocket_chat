import types from './types';
import {Message} from '../screens/Chatroom';

export interface ChatState {
  messages: Message[];
}

const initialState: ChatState = {
  messages: [],
};

export default function reducer(state = initialState, action: any): ChatState {
  switch (action.type) {
    case types.SET_NEW_MESSAGE:
      return {
        ...state,
        messages: [action.payload, ...state.messages],
      };
    default:
      return state;
  }
}
